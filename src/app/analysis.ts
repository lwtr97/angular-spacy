interface Arc {
  dir: string,
  end: number,
  label: string,
  start: number,
  text: string
};

interface Word {
  tag: string,
  text: string
};

export interface Analysis {
  arcs: Arc[],
  words: Word[]
};