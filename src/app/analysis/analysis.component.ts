import { Component, Input, OnInit } from '@angular/core';
import { Analysis } from '../analysis';

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.css']
})
export class AnalysisComponent {
  @Input() analysis: Analysis;

  constructor() {
  }

}
