import { Component, OnInit } from '@angular/core';
import { Analysis } from '../analysis';

import { Language, SpacyService } from '../spacy.service'

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
  text: string = localStorage.getItem('text') || "";
  language: Language = 'de';
  analysis: Analysis;

  constructor(
    private spacyService: SpacyService
  ) { }

  ngOnInit(): void {
  }

  analyze() {
    if (!this.text.length || this.text.length > 1000) return;

    localStorage.setItem('text', this.text);

    this.spacyService.analyze(this.text, this.language)
      .subscribe((res) => this.analysis = res)
  }

}
