import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { Analysis } from './analysis';

export type Language = 'de' | 'en';

@Injectable({
  providedIn: 'root'
})
export class SpacyService {
  private url = '/api/dep';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient) { }


  analyze(text: string, language: Language): Observable<Analysis> {
    return this.http.post<Analysis>(
      this.url, { text, model: language }, this.httpOptions
    ).pipe(catchError(this.handleError<Analysis>('analyze')));
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
